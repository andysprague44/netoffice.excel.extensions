# NetOffice.Excel.Extensions#

This project is a small library that provides some utility extension methods to make working with NetOffice.Excel easier and more intuitive.
It is mostly created as a supplement to my blog which can be found at https://andysprague.com/2017/04/25/excel-dna-custom-ribbon-cheat-sheet/

Example:

    Application excel = new Application();
    Workbook workbook = excel.Workbooks.Add("myWorkbook");
    Worksheet sheet = excel.ActiveWorkbook.GetActiveWorksheet();
    decimal cellContents = sheet.Range("A1").GetCellContents<decimal>();


### What is this repository for? ###

* Extension methods for common read and write operations when interacting with Excel from C#
### Contribution guidelines ###

* Feel free to copy methods that are useful for you, or reference the project directly.



### Who do I talk to? ###

* andy.sprague44@gmail.com

###LICENSE###

The MIT License (MIT)

Copyright (c) 2017 NetOffice.Excel.Extensions

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.