﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetOffice.Excel.Extensions.Extensions;
using NUnit.Framework;

namespace NetOffice.Excel.Extensions.Tests.Extensions
{
    [TestFixture]
    public class ExcelWriteExtensionsTests : ExcelTestBase
    {
        [Test]
        public void Test_CanReadBoolean()
        {
            WithExcel(excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet(); 
                var cell = sheet.Range("A1");
                cell.Value = "TRUE";

                var result = cell.GetCellContents<bool>();
                Assert.True(result, "Cell should read as true");
            });
        }

        [Test]
        public void Test_CanReadDate()
        {
            WithExcel(excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet(); 
                var cell = sheet.Range("A1");

                var dateStr = "2015-09-01 00:00:00";
                cell.NumberFormat = "yyyy-MM-dd HH:mm:ss";
                cell.Value = dateStr;
                
                var result = cell.GetCellContents<DateTime>();
                Assert.AreEqual(DateTime.Parse(dateStr), result, "Cell should read as date");
            });
        }

        [Test]
        public void Test_CanReadDateFormattedAsText()
        {
            WithExcel(excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet();
                var cell = sheet.Range("A1");

                var dateStr = "2015-09-01 00:00:00";
                cell.NumberFormat = "@";
                cell.Value = dateStr;

                var result = cell.GetCellContents<DateTime>();
                Assert.AreEqual(DateTime.Parse(dateStr), result, "Cell should read as date");
            });
        }

        [Test]
        public void Test_CanReadDateFormattedFromFile()
        {
            WithExcelWorkbookFromResources("NetOffice.Excel.Extensions.Tests.Resources", "DateFormats.xlsx", excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet();
                var shortDateFormat = sheet.Range("A1");
                var longDateFormat = sheet.Range("A2");
                var textFormat = sheet.Range("A3");

                var expectedDate = DateTime.Parse("2015-02-01 00:00:00");

                Assert.AreEqual(expectedDate, shortDateFormat.GetCellContents<DateTime>(), "shortDateFormat should read as date");
                Assert.AreEqual(expectedDate, longDateFormat.GetCellContents<DateTime>(), "longDateFormat should read as date");
                Assert.AreEqual(expectedDate, textFormat.GetCellContents<DateTime>(), "textFormat should read as date");
            });
        }

        [Test]
        public void Test_CanReadDecimal()
        {
            WithExcel(excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet(); 
                var cell = sheet.Range("A1");

                const decimal dec = 13.234m;
                cell.Value = (double) dec;

                decimal result = cell.GetCellContents<decimal>();
                Assert.AreEqual(dec, result, "Cell should read as decimal");
            });
        }

        [Test]
        public void Test_CanReadNumericalValue()
        {
            WithExcel(excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet(); 
                var cell = sheet.Range("A1");

                const int intValue = 13;
                cell.Value = (double)intValue;

                var result = cell.GetCellContents<int>();
                Assert.AreEqual(intValue, result, "Cell should read as int");
            });
        }

        [Test]
        public void Test_CanReadNumericalValueFormattedAsText()
        {
            WithExcel(excel =>
            {
                var sheet = excel.ActiveWorkbook.GetActiveWorksheet();
                var cell = sheet.Range("A1");

                const int intValue = 13;
                cell.Value = (double)intValue;
                cell.NumberFormat = "@";

                var result = cell.GetCellContents<int>();
                Assert.AreEqual(intValue, result, "Cell should read as int");
            });
        }


    }
}