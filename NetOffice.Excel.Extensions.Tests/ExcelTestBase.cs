﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NetOffice.Excel.Extensions.Extensions;
using NetOffice.ExcelApi;
using NUnit.Framework;
using ExcelApplication = NetOffice.ExcelApi.Application;

namespace NetOffice.Excel.Extensions.Tests
{
    public class ExcelTestBase
    {
        public T[] GetValuesFromVertical2DRange<T>(Range range)
        {
            var sourceArray = (object[,]) range.Value;
            if (sourceArray.GetLength(1) != 1)
            {
                throw new Exception("Values are not a vertical 2D array");
            }

            var rowCount = sourceArray.GetLength(0);
            var result = new T[rowCount];

            for (var i = 0; i < rowCount; i++)
            {
                result[i] = (T) sourceArray[i + 1, 1]; //of course Excel uses 1-based arrays, what did you expect?
            }
            return result;
        }


        /// <summary>
        /// Invoke a test using an excel instance.
        /// Ensures all workbooks are closed after the test.
        /// </summary>
        public void WithExcel(Action<ExcelApplication> code)
        {
            using (var excel = new ExcelApplication())
            {
                var workbook = excel.Workbooks.Add();
                Assert.True(workbook.ActiveSheet != null);

                try
                {
                    code(excel);
                }
                finally
                {
                    //Invoked explicitly as the dispose method does not close the workbook
                    foreach (var wb in excel.Workbooks)
                    {
                        wb.Close(false);
                    }
                }
            }
        }
       
        /// <summary>
        /// Invoke a test using an excel workbook loaded from test resources.
        /// Ensures all workbooks are closed after the test.
        /// </summary>
        public void WithExcelWorkbookFromResources(string resourceFolder, string resourceFileName,
            Action<ExcelApplication> code)
        {
            var excelFile = CreateExcelFileFromResources(resourceFolder, resourceFileName);

            using (var excel = new ExcelApplication())
            {
                var workbook = excel.Workbooks.Open(excelFile);
                workbook.Activate();
                try
                {
                    code(excel);
                }
                finally
                {
                    //Invoked explicitly as the dispose method does not close the workbook
                    foreach (var wb in excel.Workbooks)
                    {
                        wb.Close(false);
                    }

                    File.Delete(excelFile);
                }
            }
        }

        private static string CreateExcelFileFromResources(string resourceFolder, string resourceFileName)
        {
            var tmpDir = $@"{Path.GetTempPath()}\{Guid.NewGuid()}";
            Directory.CreateDirectory(tmpDir);
            var tmpFileName = $@"{tmpDir}\{resourceFileName}";

            var assembly = Assembly.GetExecutingAssembly();
            var fileResourcePath = $@"{resourceFolder}.{resourceFileName}";
            using (var stream = assembly.GetManifestResourceStream(fileResourcePath))
            using (var fileStream = File.Create(tmpFileName))
            {
                stream.CopyTo(fileStream);
            }
            return tmpFileName;
        }
    }
}