﻿using NetOffice.ExcelApi;
using NetOffice.OfficeApi;
using NetOffice.OfficeApi.Enums;
using System;
using System.Collections.Generic;

namespace NetOffice.Excel.Extensions.Extensions
{
    public static class ExcelPropertyExtensions
    {
        /// <summary>
        /// Add a custom document property.
        /// Should be used with an enum of available keys for your application.
        /// </summary>
        /// <example>
        /// workbook.AddDocumentProperty(MyApDocumentPropertiesEnum.ApplicationVersion, "1.0.0")
        /// </example>
        public static void AddDocumentProperty<T>(this Workbook workbook, T key, string value) where T : struct
        {
            var workbookProperties = workbook.CustomDocumentProperties as DocumentProperties;
            workbookProperties.Add(linkToContent: false, type: MsoDocProperties.msoPropertyTypeString,
                name: key.ToString(), value: value);
        }

        /// <summary>
        /// Get a custom document property.
        /// Should be used with an enum of available keys for your application.
        /// </summary>
        public static string GetDocumentProperty<T>(this Workbook workbook, T key)
        {
            var workbookProperties = workbook.CustomDocumentProperties as DocumentProperties;

            try
            {
                return (string) workbookProperties[key.ToString()].Value;
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException($"Could not get document property '{key}'", ex);
            }
        }
    }
}