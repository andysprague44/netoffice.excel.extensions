﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;

namespace NetOffice.Excel.Extensions.Extensions
{
    public static class ExcelWorkbookExtensions
    {
        /// <summary>
        /// Add a new worksheet to the end of the workbook
        /// </summary>
        public static Worksheet AddNewWorksheetToEnd(this Workbook workbook, string name, bool isHidden = false)
        {
            var newTab = (Worksheet)workbook.Worksheets.Add(
                before: Missing.Value,
                after: workbook.Worksheets.OfType<Worksheet>().Last()
            );
            newTab.Name = name;
            if (isHidden)
            {
                newTab.Visible = XlSheetVisibility.xlSheetVeryHidden;
            }
            return newTab;
        }

        /// <summary>
        /// Add a new worksheet to the start of the workbook
        /// </summary>
        public static Worksheet AddNewWorksheetToStart(this Workbook workbook, string name, bool isHidden = false)
        {
            var newTab = (Worksheet)workbook.Worksheets.Add(
                before: workbook.Sheets[1]
            );
            newTab.Name = name;
            if (isHidden)
            {
                newTab.Visible = XlSheetVisibility.xlSheetVeryHidden;
            }
            return newTab;
        }

        //TODO breaks if there are hidden worksheets
        /// <summary>
        /// Copy a worksheet to the end of the current workbook
        /// </summary>
        public static Worksheet CopyWorksheetToEnd(this Workbook workbook, Worksheet worksheetToCopy,
            string name, bool isHidden = false)
        {
            worksheetToCopy.Copy(
                before: Missing.Value,
                after: workbook.Sheets[workbook.Sheets.Count]
            );
            var copiedTab = workbook.Worksheets.OfType<Worksheet>().Last();
            copiedTab.Name = name;
            if (isHidden)
            {
                copiedTab.Visible = XlSheetVisibility.xlSheetHidden;
            }
            return copiedTab;
        }

        /// <summary>
        /// Get all Worksheets for a workbook.  Converts to Worksheet type.
        /// </summary>
        public static IEnumerable<Worksheet> GetWorksheets(this Workbook workbook, bool includeHidden = false)
        {
            return includeHidden
                ? workbook.Worksheets.OfType<Worksheet>()
                : workbook.Worksheets.OfType<Worksheet>().Where(w => w.Visible == XlSheetVisibility.xlSheetVisible);
        }

        /// <summary>
        /// Gets active worksheet for a workbook.  Converts to Worksheet type.
        /// </summary>
        public static Worksheet GetActiveWorksheet(this Workbook workbook)
        {
            return (Worksheet) workbook.ActiveSheet;
        }
        
        /// <summary>
        /// Get a worksheet by name
        /// </summary>
        public static Worksheet Worksheet(this Workbook workbook, string name)
        {
            return workbook.Worksheets.OfType<Worksheet>().SingleOrDefault(ws => ws.Name == name);
        }

        /// <summary>
        /// Get a worksheet by codeName. CodeName is unchanged if the tab name is changed in Excel so
        /// is safer for many operations. codeName will be "sheet1", "sheet2" etc.
        /// </summary>
        public static Worksheet WorksheetByCodeName(this Workbook workbook, string codeName)
        {
            return workbook.Worksheets.OfType<Worksheet>().SingleOrDefault(ws => ws.CodeName == codeName);
        }
    }
}
