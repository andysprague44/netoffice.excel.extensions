﻿using System;
using System.Collections.Generic;
using System.Linq;
using NetOffice.Excel.Extensions.Exceptions;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;

namespace NetOffice.Excel.Extensions.Extensions
{
    public static class CellValidationExtensions
    {
        /// <summary>
        /// Add validation (i.e. a drop-down list) to a cell using a static list of options.
        /// The size of the list is restricted to 255 characters, for longer lists use AddCellListValidation
        /// specifying a range of values saved elsewhere in the workbook (for example on a hidden worksheet).
        /// </summary>
        /// <example>
        ///     var cell = worksheet.Range("A1");
        ///     var initialValue = 1;
        ///     var allowedValues = new List(){1, 2, 3, 4, 5};
        ///     cell.AddCellListValidation(allowedValues, initialValue);
        /// </example>
        /// <param name="cell">Single cell to add validation to</param>
        /// <param name="allowedValues">The list of values to add, limited to 255 characters</param>
        /// <param name="initialValue">Optional initial value, default is first item in the list of allowedValues</param>
        public static void AddCellListValidation(this Range cell, IList<string> allowedValues, string initialValue = null)
        {
            var flatList = allowedValues.Aggregate((x, y) => $"{x},{y}");
            if (flatList.Length > 255)
            {
                throw new ArgumentException("Combined number of chars in the list of allowedValues can't exceed 255 characters");
            }
            cell.AddCellListValidation(flatList, initialValue);
        }

        /// <summary>
        /// Add validation (i.e. a drop-down list) to a cell using a list of options specified in a range of cells elsewhere in the workbook.
        /// </summary>
        /// <example>
        ///     var cell = worksheet.Range("A1");
        ///     var initialValue = 1;
        ///     var allowedValues = new List(){1, 2, 3, 4, 5};
        ///     var allowedValuesRange = worksheet.Range("A2:E2");
        ///     allowedValuesRange.Value2 = allowedValues.ToArray();
        ///     cell.AddCellListValidation(allowedValuesRange, initialValue);
        /// </example>
        /// <param name="cell">Single cell to add validation to</param>
        /// <param name="allowedValuesRange">A range that contains the list of values to add to the validation list</param>
        /// <param name="initialValue">Optional initial value, default is first item in the list of allowedValues</param>
        public static void AddCellListValidation(this Range cell, Range allowedValuesRange, string initialValue = null)
        {
            var fullAddress = $"='{allowedValuesRange.Worksheet.Name}'!{allowedValuesRange.Address}";
            cell.AddCellListValidation(fullAddress, initialValue);
        }

        /// <summary>
        /// Save a list of values to use for validation to a vertical column in a reference worksheet, which may be a hidden worksheet.
        /// Use in conjunction with AddCellListValidation to add validation of large lists.
        /// </summary>
        /// <param name="headerCell">The address of the first cell to save the list to, e.g. ws.Range("A1"), 
        ///     the values are stored vertically below this cell (inclusive)</param>
        /// <param name="allowedValues">The list of allowed values</param>
        /// <returns></returns>
        public static Range PopulateValidationRange(this Range headerCell, IList<string> allowedValues)
        {
            var ws = headerCell.Worksheet;
            var validationRange = ws.Range(headerCell.Address).Resize(rowSize: allowedValues.Count);
            validationRange.Cells.Value2 = ws.Application.WorksheetFunction.Transpose(allowedValues.ToArray());
            return validationRange;
        }

        private static void AddCellListValidation(this Range cell, string formula, string initialValue = null)
        {
            cell.Validation.Delete();
            cell.Validation.Add(
                XlDVType.xlValidateList,
                XlDVAlertStyle.xlValidAlertInformation,
                XlFormatConditionOperator.xlBetween,
                formula,
                Type.Missing);
            cell.Validation.IgnoreBlank = true;
            cell.Validation.InCellDropdown = true;
            if (initialValue != null)
            {
                cell.Value = initialValue;
            }
        }
    }
}
