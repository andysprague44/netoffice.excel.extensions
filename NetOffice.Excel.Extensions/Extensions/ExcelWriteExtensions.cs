﻿using System;
using System.Collections.Generic;
using System.Linq;
using NetOffice.Excel.Extensions.Exceptions;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;

namespace NetOffice.Excel.Extensions.Extensions
{
    public static class ExcelWriteExtensions
    {
        /// <summary>
        /// Insert new entire columns before the current range.
        /// </summary>
        public static void InsertColumns(this Range startRange, int numberOfExtraColumns)
        {
            var addColsRange = startRange.Worksheet.Range(
                startRange,
                startRange.Offset(rowOffset: 0, columnOffset: numberOfExtraColumns)
            ).EntireColumn;
            addColsRange.Insert(XlInsertShiftDirection.xlShiftToRight);
        }

        /// <summary>
        /// Write a 2D array to a vertical range i.e. a column.
        /// A 2D array is assumed to be horizontal i.e. a row, this method transposes and writes the array.
        /// </summary>
        public static void WriteVerticalArray(this Range range, object[] values, bool overrideCellValues = false)
        {
            var fullRange = range.Resize(rowSize: 0, columnSize: values.Length);
            if (!overrideCellValues && !fullRange.Cells.All(c => string.IsNullOrEmpty((string) c.Text)))
            {
                throw new ExcelWriteException($"Non-empty cells found in destination range '{fullRange.Address}'");
            }

            fullRange.Value = fullRange.Application.WorksheetFunction.Transpose(values);
        }
    }
}