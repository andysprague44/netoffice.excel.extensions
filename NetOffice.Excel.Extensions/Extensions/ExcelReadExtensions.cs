﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetOffice.ExcelApi;
using NetOffice.ExcelApi.Enums;
using System.Globalization;

namespace NetOffice.Excel.Extensions.Extensions
{
    public static class ExcelReadExtensions
    {
        /// <summary>
        /// Get a strongly typed value from a single excel cell.
        /// </summary>
        /// <returns></returns>
        public static T GetCellContents<T>(this Range cell)
        {
            if (cell.Cells.Count != 1)
            {
                throw new ArgumentException("Can only get contents for single cell");
            }
            try
            {
                switch (Type.GetTypeCode(typeof(T)))
                {
                    case TypeCode.String:
                        return (T) cell.Text;
                    case TypeCode.Boolean:
                        return (T) Convert.ChangeType(bool.Parse(cell.Text.ToString()), typeof(T));
                    case TypeCode.DateTime:
                        var pattern = cell.NumberFormat.ToString();
                        if (pattern == "@") //Then it is saved in text format
                        {
                            pattern = DateTimeFormatHelper.GetDateTimeFormat(); //gets the date format for the current culture
                        IFormatProvider fp = new DateTimeFormatInfo { FullDateTimePattern = pattern  };
                        return (T)Convert.ChangeType(DateTime.Parse(cell.Value.ToString(), fp), typeof(T));
                        }
                        //Missing parsing a number as a date, but seems reasonable to expect this is something end users shouldn't do
                        return (T)cell.Value; //this parses all 'Date' formatted cells correctly
                    case TypeCode.Byte:
                    case TypeCode.SByte:
                    case TypeCode.UInt16:
                    case TypeCode.UInt32:
                    case TypeCode.UInt64:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                    case TypeCode.Decimal:
                    case TypeCode.Single:
                        return (T) Convert.ChangeType((double) cell.Value, typeof(T));
                    default:
                        return (T) cell.Value;
                }
            }
            catch (Exception ex)
            {
                throw new InvalidCastException(
                    $"Value in cell '{cell.Address}' should be of type {typeof(T).Name} but was '{cell.Value}'", ex);
            }
        }

        /// <summary>
        /// Get a strongly typed value from a single excel cell, or default for T if contents can't be casted as type of T
        /// </summary>
        /// <returns></returns>
        public static T? GetCellContentsOrDefault<T>(this Range cell) where T : struct
        {
            try
            {
                return cell.GetCellContents<T>();
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        /// <summary>
        /// Get a range that autocompletes in the specified direction.
        /// Mimics Ctrl + Shift + Direction in Excel, except that if the next cell
        /// after the startRange is empty, then the startRange is returned unchanged.
        /// </summary>
        public static Range GetAutoCompleteRange(this Range startRange, XlDirection direction)
        {
            Range nextCell;
            switch (direction)
            {
                case XlDirection.xlDown:
                    nextCell = startRange.Offset(1, 0);
                    break;
                case XlDirection.xlToLeft:
                    nextCell = startRange.Offset(0, -1);
                    break;
                case XlDirection.xlToRight:
                    nextCell = startRange.Offset(0, 1);
                    break;
                case XlDirection.xlUp:
                    nextCell = startRange.Offset(-1, 0);
                    break;
                default:
                    throw new ArgumentException($"{direction} is not a valid direction");
            }
            return nextCell.Value == null ||
                   nextCell.Cells.Select(c => c.Value).All(v => string.IsNullOrEmpty(v.ToString()))
                ? startRange
                : startRange.Worksheet.Range(startRange, startRange.End(direction));
        }
    }
}