﻿using System;

namespace NetOffice.Excel.Extensions.Exceptions
{
    public class ExcelWriteException : Exception
    {
        public ExcelWriteException()
        {
        }

        public ExcelWriteException(string message) : base(message)
        {
        }

        public ExcelWriteException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
